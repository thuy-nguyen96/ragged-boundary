// adopted from: ntc js (Name that Color JavaScript)
// http://chir.ag/projects/ntc

const palette = {
  codGray: '#1d1d1d',
  mercury: '#e6e6e6',
  seaBuckthorn: '#FBA928',
  carrotOrange: '#EB9918',
  manatee: '#939AA4',
  ghost: '#CDD4DA',
  punch: '#dd3333',
  finn: '#5D2555',
};

export const colors = {
  ...palette,
  // background: palette.white,
  primary: palette.seaBuckthorn,
  primaryDarker: palette.carrotOrange,
  line: palette.mercury,
  // text: palette.white,
  dim: palette.codGray,
  error: palette.punch,
};
