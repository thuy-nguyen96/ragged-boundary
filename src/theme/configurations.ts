import {Options} from 'react-native-navigation';
import {
  Assets,
  Colors,
  Constants,
  Typography,
  Spacings,
  Incubator,
} from 'react-native-ui-lib';
import {colors} from './colors';

Assets.loadAssetsGroup('icons.demo', {
  //   add: require('./assets/icons/add.png'),
  //   camera: require('./assets/icons/cameraSelected.png'),
  //   close: require('./assets/icons/close.png'),
  //   dashboard: require('./assets/icons/dashboard.png'),
  //   image: require('./assets/icons/image.png'),
  //   refresh: require('./assets/icons/refresh.png'),
  //   search: require('./assets/icons/search.png'),
});

Assets.loadAssetsGroup('images.demo', {
  //   brokenImage: require('./assets/images/placeholderMissingImage.png'),
});

Assets.loadAssetsGroup('svgs.demo', {
  //   logo: require('./assets/svgs/headerLogo.svg').default,
});

Typography.loadTypographies({
  h1: {...Typography.text40},
  h2: {...Typography.text50},
  h3: {...Typography.text60},
  body: Typography.text70,
});

Spacings.loadSpacings({
  page: Spacings.s5,
});

/* Load color palette */
Colors.loadColors(colors);

/* Dark Mode Schemes */
Colors.loadSchemes({
  light: {
    screenBG: 'transparent',
    textColor: Colors.grey10,
    moonOrSun: Colors.yellow30,
    mountainForeground: Colors.green30,
    mountainBackground: Colors.green50,
  },
  dark: {
    screenBG: Colors.grey10,
    textColor: Colors.white,
    moonOrSun: Colors.grey80,
    mountainForeground: Colors.violet10,
    mountainBackground: Colors.violet20,
  },
});

/* Components */
Incubator.TextField.defaultProps = {
  ...Incubator.TextField.defaultProps,
  preset: 'default',
};

export function getDefaultNavigationStyle(): Options {
  return {
    statusBar: {
      visible: true,
      style: 'light',
      backgroundColor: Colors.primary, // for Android
    },
    layout: {
      backgroundColor: Colors.white,
      orientation: ['portrait', 'landscape'],
    },
    topBar: {
      visible: true,
      noBorder: true, // for iOS
      elevation: 0, // for Android
      background: {
        color: Colors.primary,
      },
      title: {
        color: Colors.white,
        fontSize: Typography.text60H.fontSize,
        fontFamily: Typography.text65H.fontFamily,
      },
      subtitle: {
        color: Colors.white,
        fontSize: Typography.text80T.fontSize,
        fontFamily: Typography.text80.fontFamily,
      },
      backButton: {
        // visible: true,
        color: Colors.white,
        showTitle: Constants.isIOS ? false : undefined,
        testID: 'pop',
      },
      leftButtonColor: Colors.white,
      leftButtonDisabledColor: Colors.rgba(Colors.white, 0.6),
      rightButtonColor: Colors.white,
      rightButtonDisabledColor: Colors.rgba(Colors.white, 0.6),
    },
  };
}
