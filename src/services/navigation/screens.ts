// Ref: https://github.com/kanzitelli/rnn-starter/blob/master/src/services/navigation/screens.ts

import ExampleScreen from '../../screens/ExampleScreen';
// import StarterScreen from 'src/screens/StarterScreen';
// import SettingsScreen from 'src/screens/SettingsScreen';
// import AppUpdateScreen from 'src/screens/AppUpdateScreen';

// import {Buttons} from './buttons';
import {Options} from 'react-native-navigation';

// Here we define all information regarding screens

type Screen = {
  id: string;
  options: () => Options;
};
type ScreenName = 'example' | 'starter' | 'settings' | 'appUpdates';

const withPrefix = (s: string) => `raggedBoundary.${s}`;

const screens: Record<ScreenName, Screen> = {
  example: {
    id: withPrefix('ExampleScreen'),
    options: () => ({
      topBar: {
        title: {text: 'Example'},
      },
    }),
  },
  starter: {
    id: withPrefix('StarterScreen'),
    options: () => ({
      topBar: {
        title: {text: 'Starter'},
        // rightButtons: [Buttons.Inc, Buttons.Dec],
      },
    }),
  },
  settings: {
    id: withPrefix('SettingsScreen'),
    options: () => ({
      topBar: {
        title: {text: 'Settings'},
      },
    }),
  },
  appUpdates: {
    id: withPrefix('AppUpdatesScreen'),
    options: () => ({
      overlay: {interceptTouchOutside: false},
      topBar: {visible: false},
      layout: {componentBackgroundColor: 'transparent'},
    }),
  },
};

const Screens = new Map<string, React.FC<any>>();
Screens.set(screens.example.id, ExampleScreen);
Screens.set(screens.starter.id, ExampleScreen);
Screens.set(screens.settings.id, ExampleScreen);
Screens.set(screens.appUpdates.id, ExampleScreen);

export default Screens;
export {screens};
